package models;

import annotations.Mapping;
import utils.ModelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Dept {
    private int idDept;
    private String nomDept;

    @Mapping(url = "dept-save")
    public ModelView save() {
        HashMap<String, Object> data;
        ModelView modelView = null;

        try {
            data = new HashMap<String, Object>();
            data.put("dept", this);

            modelView = new ModelView();
            // setting the URL where the controller will be  dispatched
            modelView.setURLDispatch("dept-save.jsp");

            // setting the data returned by the method
            modelView.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return modelView;
    }

    @Mapping(url = "dept-list")
    public ModelView list() throws Exception {
        HashMap<String, Object> data;
        ModelView modelView = null;

        try {
            data = new HashMap<String, Object>();
            data.put("deptList", getAll());

            modelView = new ModelView();
            // setting the URL where the controller will be  dispatched
            modelView.setURLDispatch("dept-list.jsp");

            // setting the data returned by the method
            modelView.setData(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return modelView;
    }

    public List<Dept> getAll() {
        ArrayList<Dept> deptList = new ArrayList<Dept>();

        deptList.add(new Dept(1, "Ressources Humaines"));
        deptList.add(new Dept(2, "Approvisionnement"));
        deptList.add(new Dept(3, "Vente et Commerce"));
        deptList.add(new Dept(4, "Marketing"));

        return deptList;
    }

    public Dept(int id, String nom) {
        this.idDept = id;
        this.nomDept = nom;
    }

    public Dept() {}

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    public int getIdDept() {
        return idDept;
    }

    public String getNomDept() {
        return nomDept;
    }
}
