<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dept - Nouveau Département</title>
</head>

<body>
    <form action="dept-save.do" method="POST">
        <label for="id-dept">ID</label>
        <input type="text" name="idDept" id="id-dept">

        <label for="nom-dept">NOM</label>
        <input type="text" name="nomDept" id="nom-dept">

        <input type="submit" value="SAVE">
    </form>
</body>
</html>
