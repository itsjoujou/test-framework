<%@ page import="models.Dept" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dept - Liste Départements</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
            </tr>
        </thead>
        <tbody>
        <% List<Dept> depList = (ArrayList<Dept>) request.getAttribute("deptList"); %>
        <% for (Dept d : depList) { %>
            <tr>
                <td><%= d.getIdDept() %></td>
                <td><%= d.getNomDept() %></td>
            </tr>
        <% } %>
        </tbody>
    </table>
</body>
</html>
