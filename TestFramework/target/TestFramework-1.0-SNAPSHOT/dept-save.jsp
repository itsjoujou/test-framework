<%@ page import="models.Dept" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dept - Détail Département</title>
</head>

<body>
    <% Dept d = (Dept) request.getAttribute("dept"); %>
    <p>ID: <%= d.getIdDept() %></p>
    <p>Nom: <%= d.getNomDept() %></p>
</body>
</html>
